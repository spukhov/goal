package com.pushock.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.text.SimpleDateFormat;
import java.util.Date;

@Document(collection = "tickets")
public class Ticket implements Model {
    @Id
    private String id;
    private String ticketNumber;
    private String title;
    private String projectId;
    private String description;
    private Date reportedDate;
    private String status;
    private String priority;
    private String reporterId;
    private String assigneeId;

    public Ticket(){
    }

    public enum Status {
        OPEN("OPEN"),
        IN_PROGRESS("IN_PROGRESS"),
        DONE("DONE"),
        UNKNOWN("UNKNOWN");

        private String statusValue;

        private Status (String value) {
            statusValue = value;
        }

        public String getStatusValue() {
            return statusValue;
        }

        public Status fromString (String s){
            for (Status status: Status.values()) {
                if (status.getStatusValue().equals(s)) {
                    return status;
                }
            }
            throw new RuntimeException("unknown status");
        }
    }

    public enum Priority {
        BLOCKER("BLOCKER"),
        CRITICAL("CRITICAL"),
        MAJOR("MAJOR"),
        MINOR("MINOR"),
        TRIVIAL("TRIVIAL");

        private String priorityValue;

        private Priority (String value) {
            priorityValue = value;
        }

        public String getPriorityValue() {
            return priorityValue;
        }

        public Priority fromString (String s){
            for (Priority priority: Priority.values()) {
                if (priority.getPriorityValue().equals(s)) {
                    return priority;
                }
            }
            throw new RuntimeException("unknown priority");
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getReportedDate() {
        return reportedDate;
    }

    public String getReportedDateFormatted(){
        SimpleDateFormat sdf = new SimpleDateFormat("MMM, dd yyyy (HH:mm)");
        return sdf.format(reportedDate) ;
    }

    public void setReportedDate(Date reportedDate) {
        this.reportedDate = reportedDate;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getReporterId() {
        return reporterId;
    }

    public void setReporterId(String reporterId) {
        this.reporterId = reporterId;
    }

    public String getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(String assigneeId) {
        this.assigneeId = assigneeId;
    }
}
