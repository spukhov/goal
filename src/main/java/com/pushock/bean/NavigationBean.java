package com.pushock.bean;

import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;

@Component
@ManagedBean(name = "navigationBean")
public class NavigationBean {

    public String navigateToProject() {
        return "project";
    }

    public String navigateToUser() {
        return "user";
    }

    public String navigateToProfile() {
        return "profile";
    }

    public String navigateToTicket() {
        return "ticket";
    }

    public String navigateToDashboard() {
        return "dashboard";
    }
}
