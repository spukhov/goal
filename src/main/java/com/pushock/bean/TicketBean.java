package com.pushock.bean;

import com.pushock.dao.ProjectDAO;
import com.pushock.dao.TicketDAO;
import com.pushock.model.Project;
import com.pushock.model.Ticket;
import com.pushock.util.SessionManager;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Component
@ManagedBean
@RequestScoped
public class TicketBean implements Serializable {
    private String title;
    private String description;

    @Autowired
    private TicketDAO ticketDAO;
    @Autowired
    private ProjectDAO projectDAO;

    public List<Ticket> findTicketsByProjectId(String projectId) {
         return ticketDAO.findTicketsByProjectId(projectId);
    }

    public void createTicket() {
        Project project =  (Project) SessionManager.getAttribute("currentProject");
        Ticket ticket = new Ticket();
        ticket.setTitle(title);
        ticket.setDescription(description);
        ticket.setProjectId(project.getId());
        ticket.setReportedDate(new Date());
        ticket.setStatus(Ticket.Status.OPEN.getStatusValue());

        String abbr = project.getAbbreviation();
        if (abbr==null) {
            abbr = project.getName().substring(0,3);
        }
        ticket.setTicketNumber(abbr + "-" + getLastTicketNumberInProject(project.getId()));
        ticketDAO.save(ticket);
    }

    private int getLastTicketNumberInProject(String projectId) {
        List<Ticket> tickets = ticketDAO.findTicketsByProjectId(projectId);
        int number = 1;
        if (!tickets.isEmpty()) {
            number = Integer.valueOf(tickets.get(tickets.size() - 1).getTicketNumber().split("-")[1]) + 1;
        }
        return number;
    }

    public void setSessionAttrs(Ticket currentProject){
        SessionManager.setAttribute("currentTicket", currentProject);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
