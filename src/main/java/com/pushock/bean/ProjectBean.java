package com.pushock.bean;

import com.pushock.dao.ProjectDAO;
import com.pushock.dao.UserDAO;
import com.pushock.model.Project;
import com.pushock.model.User;
import com.pushock.util.SessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.List;

@Component
@ManagedBean(name = "projectBean")
@RequestScoped
@Scope("request")
public class ProjectBean implements Serializable{
    private String id;
    private String name;
    private String description;
    private String abbreviation;
    private List<String> userIds;
    private List<String> ticketIds;
    private String usersToAdd;

    @Autowired
    private ProjectDAO projectDAO;
    @Autowired
    private UserDAO userDAO;

    public ProjectBean() { }

    public void createProject(){
        User currentUser = (User) SessionManager.getAttribute("currentUser");
        Project project = new Project();
        project.setName(this.name);
        project.setDescription(this.description);
        project.setAbbreviation(this.abbreviation);
        project.setUserIds(new String[]{currentUser.getId()});
        projectDAO.save(project);
        System.out.println("created project id " + project.getId());
        userDAO.addProjectIdForUser(project.getId(), ((User) SessionManager.getAttribute("currentUser")).getEmail());
    }

    public void addUsersToProject(){
        for (String s : usersToAdd.split(",")){
            if (userDAO.checkUserExistenceByEmail(s)){
                userDAO.addProjectIdForUser(((Project)SessionManager.getAttribute("currentProject")).getId(), s);
                //projectDAO.addUserIdByEmail(s);
            } else {
                // GENERATE ERROR MESSAGE
            }
        }
    }

    public Project findNameById(String id) {
        return projectDAO.findNameById(id);
    }

    public List<Project> getProjects(){
        return projectDAO.findAll();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }

    public String getUsersToAdd() {
        return usersToAdd;
    }

    public void setUsersToAdd(String usersToAdd) {
        this.usersToAdd = usersToAdd;
    }
}
