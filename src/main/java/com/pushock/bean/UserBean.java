package com.pushock.bean;

import com.pushock.dao.ProjectDAO;
import com.pushock.dao.UserDAO;
import com.pushock.model.Project;
import com.pushock.model.User;
import com.pushock.util.SessionManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;

@Component
@ManagedBean (name = "userBean")
@SessionScoped
@Scope("session")
public class UserBean implements Serializable {

    private String id;
    private String email;
    private String password;
    private String firstname;
    private String secondname;

    private boolean isLoggedIn = false;

    @Autowired
    UserDAO userDao;
    @Autowired
    ProjectDAO projectDao;

    public UserBean() {
    }

    @PostConstruct
    public void initValues(){

    }

    public String register() {
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        user.setFirstname(firstname);
        user.setSecondname(secondname);
        userDao.register(user);

        return validateLogin();
    }

    public String validateLogin() {
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        if (userDao.validateLogin(user)) {
            System.out.println("TRUUUE");
            isLoggedIn = true;
            return "dashboard";
        } else {
            System.out.println("FAAALSE");
            FacesContext.getCurrentInstance()
                    .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Autorization failed", null));
            isLoggedIn = false;
            return null;
        }
    }

    public List<User> getProjectUsers() {
        List<User> users = userDao.findUsersByProjectId(((Project) SessionManager.getAttribute("currentProject")).getId());
        return users;
    }

    public void setSessionAttrs(Project currentProject){
        SessionManager.setAttribute("currentProject", currentProject);
    }

    public String logout() {
        SessionManager.invalidate();
        return "/login.xhtml";
    }

    public void findUserByName(String email) {
        User user = userDao.findUserByName(email);
        System.out.println(user.getEmail());

    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public String getId() {
        return ((User)SessionManager.getAttribute("user")).getId().toString();
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

}
