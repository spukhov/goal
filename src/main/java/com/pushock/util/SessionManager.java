package com.pushock.util;

import org.springframework.stereotype.Component;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

@Component
public class SessionManager {
    private static HttpSession session;


    public static Object getAttribute(String attrName) {
        session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().
                getSession(false);
        return session.getAttribute(attrName);
    }

    public static void setAttribute(String attrName, Object attrValue)  {
        session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().
                getSession(false);
        session.setAttribute(attrName, attrValue);
    }

    public static void invalidate() {
        session.invalidate();
    }
}
