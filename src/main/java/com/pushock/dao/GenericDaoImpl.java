package com.pushock.dao;


import com.mongodb.WriteResult;
import com.pushock.config.SpringMongoConfig;
import com.pushock.model.Model;
import com.pushock.model.User;
import org.bson.types.ObjectId;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

public class GenericDaoImpl<T extends Model> implements GenericDao<T> {
    ApplicationContext ctx =
            new AnnotationConfigApplicationContext(SpringMongoConfig.class);
    MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
    private Class< T > type;

    public GenericDaoImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }
    public void save(T t) {
        System.out.println("Generic save " + t.getClass());
        mongoOperation.save(t);
    }

    public void delete(final T t) {
        mongoOperation.remove(t);
    }

    public List<T> findAll() {
        System.out.println("Generic findAll " + mongoOperation.findAll(type));
        return mongoOperation.findAll(type);
    }

    public void update (String id, String name, Object value) {
        Query query = new Query(Criteria.where("_id").is(id));
        Update update = new Update();
        update.addToSet(name, value);
        WriteResult result = mongoOperation.updateFirst(query, update, type);
        System.out.println(result);
    }


    public T findByFieldValue (String fieldName, String value) {
        return findByFieldValue(fieldName, value);
    }

}
