package com.pushock.dao;

import com.pushock.model.Ticket;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TicketDAO extends GenericDaoImpl<Ticket> {
    public List<Ticket> findTicketsByProjectId(String projectId) {
        Query query = new Query(Criteria.where("projectId").is(projectId));
        System.out.println("tickets (" + projectId +  "): " + mongoOperation.find(query, Ticket.class));
        return mongoOperation.find(query, Ticket.class);
    }
}
