package com.pushock.dao;

import com.pushock.model.Project;
import com.pushock.model.User;
import com.pushock.util.SessionManager;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.List;

@Repository
public class ProjectDAO extends GenericDaoImpl<Project> {
    @Override
    public List<Project> findAll() {
        String userId = ((User) SessionManager.getAttribute("currentUser")).getId();
        Query query = new Query(Criteria.where("userIds").in(new String[]{userId}));
        System.out.println("userId: " + userId);
        System.out.println(mongoOperation.find(query, Project.class));
        return mongoOperation.find(query , Project.class);
    }

    public Project findByName(String title){
        Query query = new Query(Criteria.where("title").is(title));
        return mongoOperation.findOne(query, Project.class);
    }

    public Project findNameById(String id) {
        Query query = new Query(Criteria.where("_id").is(id));
        return mongoOperation.findOne(query, Project.class);
    }
    public void addUser(String email){

    }
}
