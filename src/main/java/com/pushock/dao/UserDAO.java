package com.pushock.dao;

import com.pushock.model.Project;
import com.pushock.model.User;
import com.pushock.util.SessionManager;
import com.sun.org.apache.xpath.internal.SourceTree;
import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;

@Repository
public class UserDAO extends GenericDaoImpl<User>{
    public static Logger LOG = Logger.getLogger(UserDAO.class);

    public void register(User user){
        LOG.info("register()");
         mongoOperation.save(user);
    }

    public boolean validateLogin(User user){
        LOG.info("validateLogin() - email: " + user.getEmail() + "; password: " + user.getPassword());
        Query searchUserQuery = new Query(Criteria.where("email").is(user.getEmail()).and("password").is(user.getPassword()));
        User currentUser = mongoOperation.findOne(searchUserQuery, User.class);
        if (currentUser!=null) {
            String currentUserId = currentUser.getId();
            SessionManager.setAttribute("currentUser", currentUser);
            LOG.info("currentUserId: "+currentUserId);
            return true;
        }
        else return false;
    }


    public void addProjectIdForUser(String projectId, String userEmail) {
        User currentUser = (User) SessionManager.getAttribute("currentUser");
        List <String> projectIds = currentUser.getProjectIds();
        if (projectIds==null){
            update(getIdByEmail(userEmail), "projectIds", projectId);
        }  else {
            update(getIdByEmail(userEmail), "projectIds", projectId);
            System.out.println("addProjectIdForUser with email: " + userEmail);
        }

    }

    
    public User findUserByName(String email)
    {
    	Query query = new Query(Criteria.where("email").is(email));
		return mongoOperation.findOne(query, User.class);
    	
    }

    public List<User> findUsersByProjectId(String projectId) {
        Query query = new Query(Criteria.where("projectIds").in(new String[]{projectId}));
        System.out.println("users (" + projectId +  "): " + mongoOperation.find(query, User.class));
        return mongoOperation.find(query, User.class);
    }

    public String getIdByEmail(String email){
        Query query = new Query(Criteria.where("email").is(email));
            User user = mongoOperation.findOne(query, User.class);
            return user.getId();
    }

    public boolean checkUserExistenceByEmail(String email){
        Query query = new Query(Criteria.where("email").is(email));
        boolean exists = mongoOperation.find(query, User.class)==null;
        return exists;
    }
}
