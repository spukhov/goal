package com.pushock.dao;

import java.util.List;

public interface GenericDao<T> {
    void save(T t);
    void delete(T t);
    public List<T> findAll();
    public T findByFieldValue(String fieldName, String value);
}